# Non-mechanical steering OCT

This repo contains the experimental data used to produce the figures in the paper

    Non-mechanical steering of the optical beam in spectral-domain Optical Coherence Tomography. Daniel F. Urrego, Gerard J. Machado, Juan P. Torres 

The collection consists of the following:

    20210125_LiquidCrystal  has the data of the characterization of the liquid crystal variable retarder (LCVR).

    20210128_decay	has the data of the stepped phase shifting correction.

    20210129_sample1 has the data of the sample #1.

    20210201_sample2 has the data of the sample #2.

    210201b_sample3 has the data of the sample #3.


This collection is presented on an as-is manner in the hope that it will be useful.

The copyright of this collection rests with the authors (2024). It is made available under the Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0) license. For any academic use that results in a publication, please cite the main paper.